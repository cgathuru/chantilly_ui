import React from 'react';

class About extends React.Component {


	render(){
		console.log(this.props);
		const {title} = this.props.match.params;
		return (
			<h1>{title}</h1>
		);
	}
}

export default About;
