const notificationReducer = (state={}, action) => {
	switch(action.type){
		case 'ADD_NOTIFICATION':
			if (action.notification.level === 'error') {
				return {...action.notification, autoDismiss: 0};
			} else {
				return action.notification;
			}
		default:
			return state;
	}
}

const articleReducer = (state='', action) => {
	switch(action.type){
		case 'ADD_CONTENT':
			return action.content;
		default:
			return state;
	}
}

export {
	notificationReducer, articleReducer
};
