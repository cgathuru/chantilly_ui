import React from 'react';
import {Navbar, NavItem, Nav, MenuItem, NavDropdown} from 'react-bootstrap';
import {LinkContainer} from 'react-router-bootstrap';
import {Route, Switch} from 'react-router-dom';
import Home from './Home';
import Article from './Article';

const Menu = () => (
	<div>
		<Navbar inverse collapseOnSelect>
			<Navbar.Header>
				<Navbar.Brand>
					<LinkContainer to="/">
						<a>Chantilly Schools</a>
					</LinkContainer>
				</Navbar.Brand>
				<Navbar.Toggle />
			</Navbar.Header>
			<Navbar.Collapse>
				<Nav>
					<LinkContainer to="/" extact={true}>
						<NavItem eventKey={1}>Home</NavItem>
					</LinkContainer>
					<NavDropdown eventKey={2} title="About" id="about">
						<LinkContainer to="/about-chantilly-schools">
							<MenuItem eventKey={2.1}>About Chantilly Schools</MenuItem>
						</LinkContainer>
						<LinkContainer to="/directors-message">
							<MenuItem eventKey={2.2}>Directors Message</MenuItem>
						</LinkContainer>
						<LinkContainer to="/mission-vision">
							<MenuItem eventKey={2.3}>Mission & Vision</MenuItem>
						</LinkContainer>
						<LinkContainer to="/our-history">
							<MenuItem eventKey={2.4}>Our History</MenuItem>
						</LinkContainer>
						<LinkContainer to="/academics">
							<MenuItem eventKey={2.6}>Academics</MenuItem>
						</LinkContainer>
						<LinkContainer to="/key-achievments">
							<MenuItem eventKey={2.7}>Key Achievements</MenuItem>
						</LinkContainer>
						<LinkContainer to="/school-anthem">
							<MenuItem eventKey={2.8}>School Anthem</MenuItem>
						</LinkContainer>
						<LinkContainer to="/alumni">
							<MenuItem eventKey={2.9}>Alumni</MenuItem>
						</LinkContainer>
						<LinkContainer to="/fees">
							<MenuItem eventKey={2.10}>Fees</MenuItem>
						</LinkContainer>
					</NavDropdown>
					<NavDropdown eventKey={3} title="Facilities" id="facilities">
						<LinkContainer to="/transport">
							<MenuItem eventKey={3.1}>Transport</MenuItem>
						</LinkContainer>
						<LinkContainer to="/kitchen">
							<MenuItem eventKey={3.2}>Kitchen</MenuItem>
						</LinkContainer>
						<LinkContainer to="/learning-resource-centre">
							<MenuItem eventKey={3.2}>Learning Resource Centre</MenuItem>
						</LinkContainer>
						<LinkContainer to="/media">
							<MenuItem eventKey={3.3}>Media</MenuItem>
						</LinkContainer>
						<LinkContainer to="/sports">
							<MenuItem eventKey={3.4}>Sports</MenuItem>
						</LinkContainer>
						<LinkContainer to="/boarding">
							<MenuItem eventKey={3.5}>Boarding</MenuItem>
						</LinkContainer>
					</NavDropdown>
					<NavDropdown eventKey={4} title="News" id="news">
						<LinkContainer to="/recent-news">
							<MenuItem eventKey={4.1}>Recent News</MenuItem>
						</LinkContainer>
						<LinkContainer to="/school-calender">
							<MenuItem eventKey={4.2}>School Calender</MenuItem>
						</LinkContainer>
						<LinkContainer to="/gallery">
							<MenuItem eventKey={4.2}>Gallery</MenuItem>
						</LinkContainer>
						<LinkContainer to="/video-gallery">
							<MenuItem eventKey={4.2}>Video Gallery</MenuItem>
						</LinkContainer>
					</NavDropdown>
					<NavDropdown eventKey={5} title="Connect" id="connect">
						<LinkContainer to="/get-in-touch">
							<MenuItem eventKey={5.1}>Get In Touch</MenuItem>
						</LinkContainer>
						<LinkContainer to="/apply-online?prmary=true">
							<MenuItem eventKey={5.2}>Apply Online</MenuItem>
						</LinkContainer>
					</NavDropdown>
				</Nav>
				<Nav pullRight>
					<LinkContainer to="/login">
						<NavItem eventKey={1}>Login</NavItem>
					</LinkContainer>
				</Nav>
			</Navbar.Collapse>
		</Navbar>
		<Switch>
			<Route path="/" exact component={Child}/>
			<Route path="/about" exact component={Home}/>
			<Route path="/:title" component={Article}/>
			<Route exponent={Home}/>
		</Switch>
	</div>
)

const Child = ({ match }) => (
	<div>
		<h3>ID: {match.params.title}</h3>
	</div>
)

export default Menu;
