import React from 'react';
import {getArticle} from './actions';
import {connect} from 'react-redux';

class Article extends React.PureComponent {

	componentDidMount() {
		const {title} = this.props.match.params;
		this.props.dispatch(getArticle(title));
	}

	componentDidUpdate(prevProps, prevState) {
		if (!!prevProps.match && prevProps.match.params.title !== this.props.match.params.title){
			const {title} = this.props.match.params;
			this.props.dispatch(getArticle(title));
		}
	}

	render(){
		const style = {
			width: '100%',
			height: '700px'
		}
		const {title} = this.props.match.params;
		return (
			<div>
			<h1>Article {title}</h1>
			{this.props.article ? (
				<textarea value={this.props.article} style={style} readOnly/>
			): null}
			</div>
		);
	}

}

function mapStateToProps(state) {
	return state;
}
export default connect(mapStateToProps)(Article);
