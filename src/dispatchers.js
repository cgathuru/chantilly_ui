const addNotification = (notification) => {
	return {
		type: 'ADD_NOTIFICATION',
		notification: notification
	};
}

const addContent = (content) => {
	return {
		type: 'ADD_CONTENT',
		content: content
	};
}

export {addNotification, addContent};
