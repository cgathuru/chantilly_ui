function get(url){
	return fetch(url).then(checkStatus).then(getJSON);
}

function getJSON(response){
	return response.json();
}

function checkStatus(response){
	if (response.ok) {
		return Promise.resolve(response);
	} else {
		const statusText = response.statusText;
		const message = statusText.includes('Server Error') ?
			'Server encountered an Error.': statusText;
		return Promise.reject(new Error(message));
	}
}

export {
	get
};
