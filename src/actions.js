import {get} from './remote';
import {addNotification, addContent} from './dispatchers';

const getArticle = (title) => {
	return (dispatch) => {
		get(`/article/${title}/`)
			.then(({content}) => {
				const notification = {
					message: `Successfully got ${title}`,
					level: 'success'
				};
				dispatch(addContent(content));
				dispatch(addNotification(notification));
			})
			.catch((e) => {
				const message = `Unable to get article ${title}. ${e.message}`;
				dispatch(addContent(''));
				dispatch(addNotification({level: 'error', message: message}));
			});
	};
}

export {getArticle};
