import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

import {Provider} from 'react-redux';
import {createStore, combineReducers, applyMiddleware} from 'redux';
import {ConnectedRouter as Router, routerReducer, routerMiddleware} from 'react-router-redux';

import thunk from 'redux-thunk';
import createHistory from 'history/createBrowserHistory';

import {notificationReducer, articleReducer} from './reducers.js';

const history = createHistory();
const middleware = routerMiddleware(history);


const reducers = combineReducers({router: routerReducer,
	article: articleReducer, notification: notificationReducer});
const store = createStore(reducers, applyMiddleware(middleware, thunk));

ReactDOM.render(
	<Provider store={store}>
		<Router history={history}>
			<App />
		</Router>
	</Provider>,
	document.getElementById('root'));
registerServiceWorker();
