import React, { Component } from 'react';
import Menu from './Menu';
import NotificationSystem from 'react-notification-system';
import {connect} from 'react-redux';

class App extends Component {
	componentDidMount(){
		this.notificationSystem = this.refs.notificationSystem;
	}

	componentDidUpdate(nextProps, nextState){
		const {notification ={}} = this.props;
		if (Object.keys(notification).length){
			if (notification !== nextProps.notification){
				this.notificationSystem.addNotification(notification);
			}
		}
	}
	render() {
		return (
				<div className="App">
					<Menu/>
					<NotificationSystem id="notificationSystem" ref="notificationSystem" />
				</div>
		);
	}
}

function mapStateToProps(state){
	return state;
}
export default connect(mapStateToProps)(App);
